<?php

declare(strict_types=1);

namespace Grifix\ArrayWrapper;

use Grifix\ArrayWrapper\Exceptions\CannotGetAsArrayException;
use Grifix\ArrayWrapper\Exceptions\CannotGetAsBoolException;
use Grifix\ArrayWrapper\Exceptions\CannotGetAsFloatException;
use Grifix\ArrayWrapper\Exceptions\CannotGetAsIntException;
use Grifix\ArrayWrapper\Exceptions\CannotGetAsStringException;
use Grifix\ArrayWrapper\Exceptions\ElementIsNotAnArrayException;
use Traversable;

final class ArrayWrapper implements \IteratorAggregate
{
    private function __construct(private array $array)
    {
    }

    public static function create(array $array = []): self
    {
        return new self($array);
    }

    public function getLastElement(mixed $default = null): mixed
    {
        if (empty($this->array)) {
            return $default;
        }

        return $this->array[count($this->array) - 1];
    }

    public function getFirstElement(mixed $default = null): mixed
    {
        if (empty($this->array)) {
            return $default;
        }

        return $this->array[0];
    }

    public function hasElement(string|int $key, bool $keyAsPath = true): bool
    {
        return $this->doHasElement(
            self::keyToPath($key, $keyAsPath),
            $this->array
        );
    }

    private function doHasElement(array $path, $array): bool
    {
        $firstKey = array_shift($path);
        if (false === array_key_exists($firstKey, $array)) {
            return false;
        }

        if (empty($path)) {
            return true;
        }

        if (is_array($array[$firstKey])) {
            return $this->doHasElement($path, $array[$firstKey]);
        }

        return false;
    }

    public function getElement(string|int $key, $default = null, bool $keyAsPath = true): mixed
    {
        $result = $this->array;
        foreach (self::keyToPath($key, $keyAsPath) as $k) {
            if (is_array($result) && array_key_exists($k, $result) && $result[$k] !== null) {
                $result = $result[$k];
            } else {
                return $default;
            }
        }

        return $result;
    }

    public function addElement(string|int $key, mixed $value, bool $keyAsPath = true): void
    {
        if ($this->hasElement($key, $keyAsPath) && !is_array($this->getElement($key, keyAsPath: $keyAsPath))) {
            throw new ElementIsNotAnArrayException($key);
        }
        if (!$this->hasElement($key, $keyAsPath)) {
            $this->setElement($key, [$value], $keyAsPath);

            return;
        }
        $array = $this->getElement($key, keyAsPath: $keyAsPath);
        $array[] = $value;
        $this->setElement($key, $array, $keyAsPath);
    }

    public function setElement($key, $value, bool $keyAsPath = true): void
    {
        $this->doSetElement($this->array, $key, $value, $keyAsPath);
    }

    public function isAssociative(): bool
    {
        foreach (array_keys($this->array) as $key) {
            if (is_string($key)) {
                return true;
            }
        }

        return false;
    }

    private static function doSetElement(array &$array, int|string $key, $val, bool $keyAsPath = true): void
    {
        $path = self::keyToPath($key, $keyAsPath);
        $firstKey = array_shift($path);
        if (count($path)) {
            if (!isset($array[$firstKey]) || !is_array($array[$firstKey])) {
                $array[$firstKey] = [];
            }
            self::doSetElement($array[$firstKey], implode('.', $path), $val);
        } else {
            $array[$firstKey] = $val;
        }
    }

    public function getWrapped(): array
    {
        return $this->array;
    }

    public function getIterator(): Traversable
    {
        return new \ArrayIterator($this->array);
    }

    public function removeElement(string|int $key, bool $keyAsPath = true): void
    {
        self::doRemoveElement($this->array, $key, $keyAsPath);
    }

    public function containsArray(array $needle): bool
    {
        foreach ($needle as $key => $value) {
            if (!array_key_exists($key, $this->array)) {
                return false;
            }
            if (is_array($value)) {
                if (!is_array($this->array[$key])) {
                    return false;
                }
                return self::create($this->array[$key])->containsArray($value);
            }
            if ($value !== $this->array[$key]) {
                return false;
            }
        }
        return true;
    }

    private static function doRemoveElement(array &$array, int|string $key, bool $keyAsPath = true): void
    {
        $path = self::keyToPath($key, $keyAsPath);
        $firstKey = array_shift($path);
        if (count($path)) {
            if (!isset($array[$firstKey]) || !is_array($array[$firstKey])) {
                $array[$firstKey] = [];
            }
            self::doRemoveElement($array[$firstKey], implode('.', $path));
        } else {
            unset($array[$firstKey]);
        }
    }

    private static function keyToPath(string|int $key, bool $keyAsPath = true): array
    {
        if ($keyAsPath) {
            return explode('.', strval($key));
        }
        return [$key];
    }

    public function getAsStringOrNull(string|int $key, ?string $default = null): ?string
    {
        $value = $this->getElement($key, $default);

        if (is_string($value)) {
            return $value;
        }

        if (null === $value) {
            return null;
        }

        if (!$this->canBeConvertedToString($value)) {
            throw new CannotGetAsStringException($key);
        }

        return (string)$value;
    }

    private function canBeConvertedToString(mixed $value): bool
    {
        if (is_object($value)) {
            return method_exists($value, '__toString');
        }
        return is_scalar($value);
    }

    public function getAsFloatOrNull(string|int $key, ?float $default = null): ?float
    {
        $value = $this->getElement($key, $default);
        if (is_float($value)) {
            return $value;
        }
        if (null === $value) {
            return null;
        }

        if (!$this->canBeConvertedToString($value)) {
            throw new CannotGetAsFloatException($key);
        }

        $value = (string)$value;

        if (!is_numeric($value)) {
            throw new CannotGetAsFloatException($key);
        }
        return (float)$value;
    }

    public function getAsIntOrNull(string|int $key, ?int $default = null): ?int
    {
        $value = $this->getElement($key, $default);
        if (null === $value) {
            return null;
        }

        if (is_int($value)) {
            return $value;
        }

        if (!$this->canBeConvertedToString($value)) {
            throw new CannotGetAsIntException($key);
        }
        $value = (string)$value;

        if (!ctype_digit($value)) {
            throw new CannotGetAsIntException($key);
        }
        return (int)$value;
    }

    public function getAsBoolOrNull(string|int $key, ?bool $default = null): ?bool
    {
        $value = $this->getElement($key, $default);
        if (is_bool($value)) {
            return $value;
        }
        if (null === $value) {
            return null;
        }
        if (1 === $value) {
            return true;
        }
        if (0 === $value) {
            return false;
        }
        if (!$this->canBeConvertedToString($value)) {
            throw new CannotGetAsBoolException($key);
        }
        $value = (string)$value;

        if ('1' === $value) {
            return true;
        }
        if ('true' === $value) {
            return true;
        }
        if ('TRUE' === $value) {
            return true;
        }
        if ('True' === $value) {
            return true;
        }
        if ('0' === $value) {
            return false;
        }
        if ('false' === $value) {
            return false;
        }
        if ('FALSE' === $value) {
            return false;
        }
        if ('False' === $value) {
            return false;
        }
        throw new CannotGetAsBoolException($key);
    }

    public function getAsArrayOrNull(string|int $key, ?array $default = null): ?array
    {
        $value = $this->getElement($key, $default);
        if (null === $value) {
            return null;
        }
        if (!is_array($value)) {
            throw new CannotGetAsArrayException($key);
        }
        return $value;
    }

    public function getAsString(string|int $key, ?string $default = null): string
    {
        $value = $this->getAsStringOrNull($key, $default);
        if (null === $value) {
            throw new CannotGetAsStringException($key);
        }
        return $value;
    }

    public function getAsInt(string|int $key, ?int $default = null): int
    {
        $value = $this->getAsIntOrNull($key, $default);
        if (null === $value) {
            throw new CannotGetAsIntException($key);
        }
        return $value;
    }

    public function getAsFloat(string|int $key, ?float $default = null): float
    {
        $value = $this->getAsFloatOrNull($key, $default);
        if (null === $value) {
            throw new CannotGetAsFloatException($key);
        }
        return $value;
    }

    public function getAsBool(string|int $key, ?bool $default = null): bool
    {
        $value = $this->getAsBoolOrNull($key, $default);
        if (null === $value) {
            throw new CannotGetAsBoolException($key);
        }
        return $value;
    }

    public function getAsArray(string|int $key, ?array $default = null): array
    {
        $value = $this->getAsArrayOrNull($key, $default);
        if (null === $value) {
            throw new CannotGetAsArrayException($key);
        }
        return $value;
    }
}
