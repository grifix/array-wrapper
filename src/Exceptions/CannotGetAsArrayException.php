<?php

declare(strict_types=1);

namespace Grifix\ArrayWrapper\Exceptions;

final class CannotGetAsArrayException extends \RuntimeException
{

    public function __construct(string $key)
    {
        parent::__construct(sprintf('Cannot get [%s] as array!', $key));
    }
}
