<?php

declare(strict_types=1);

namespace Grifix\ArrayWrapper\Exceptions;

final class CannotGetAsFloatException extends \RuntimeException
{

    public function __construct(string $key)
    {
        parent::__construct(sprintf('Cannot get [%s] as float!', $key));
    }
}
