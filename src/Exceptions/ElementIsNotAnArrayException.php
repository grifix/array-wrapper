<?php
declare(strict_types=1);

namespace Grifix\ArrayWrapper\Exceptions;

final class ElementIsNotAnArrayException extends \Exception
{

    public function __construct(public readonly string|int $pathKey)
    {
        parent::__construct(sprintf('Element [%s] is not an array!', $pathKey));
    }
}
