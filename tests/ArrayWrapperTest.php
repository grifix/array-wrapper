<?php

declare(strict_types=1);

namespace Grifix\ArrayWrapper\Tests;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\ArrayWrapper\Exceptions\CannotGetAsArrayException;
use Grifix\ArrayWrapper\Exceptions\CannotGetAsBoolException;
use Grifix\ArrayWrapper\Exceptions\CannotGetAsFloatException;
use Grifix\ArrayWrapper\Exceptions\CannotGetAsIntException;
use Grifix\ArrayWrapper\Exceptions\CannotGetAsStringException;
use Grifix\ArrayWrapper\Exceptions\ElementIsNotAnArrayException;
use PHPUnit\Framework\TestCase;

final class ArrayWrapperTest extends TestCase
{
    /**
     * @dataProvider itGetsElementDataProvider
     */
    public function testItGetsElement(
        array $array,
        string|int $pathKey,
        mixed $expectedResult,
        mixed $defaultValue = null
    ): void {
        $wrapper = ArrayWrapper::create($array);
        self::assertEquals($expectedResult, $wrapper->getElement($pathKey, $defaultValue));
    }

    public function itGetsElementDataProvider(): array
    {
        return [
            [
                [0 => 'foo'],
                0,
                'foo'
            ],
            [
                ['name' => 'Mike'],
                'name',
                'Mike'
            ],
            [
                [
                    'user' => [
                        'name' => 'Mike'
                    ]
                ],
                'user.name',
                'Mike'
            ],
            [
                [
                    'user' => [
                        'names' => [
                            0 => 'Mike'
                        ]
                    ]
                ],
                'user.names.0',
                'Mike'
            ],
            [
                [],
                1,
                null
            ],
            [
                [],
                1,
                'test',
                'test'
            ]
        ];
    }

    /**
     * @dataProvider itSetsElementDataProvider
     */
    public function testItSetsElement(string|int $pathKey, mixed $value, array $expectedResult)
    {
        $wrapper = ArrayWrapper::create();
        $wrapper->setElement($pathKey, $value);
        self::assertEquals($expectedResult, $wrapper->getWrapped());
    }

    public function itSetsElementDataProvider(): array
    {
        return [
            [
                0,
                'Mike',
                ['Mike']
            ],
            [
                'name',
                'Mike',
                [
                    'name' => 'Mike'
                ]
            ],
            [
                'user.name',
                'Mike',
                [
                    'user' => [
                        'name' => 'Mike'
                    ]
                ]
            ],
            [
                'user.names.0',
                'Mike',
                [
                    'user' => [
                        'names' => [
                            0 => 'Mike'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @dataProvider itHasElementDataProvider
     */
    public function testItHasElement(array $array, string|int $pathKey, bool $expectedResult): void
    {
        $wrapper = ArrayWrapper::create($array);
        self::assertEquals($expectedResult, $wrapper->hasElement($pathKey));
    }

    public function itHasElementDataProvider(): array
    {
        return [
            [
                [],
                0,
                false
            ],
            [
                [0 => null],
                0,
                true
            ],
            [
                [0 => 'value'],
                0,
                true
            ],
            [
                ['user' => 'Mike'],
                'user',
                true
            ],
            [
                ['user' => 'Mike'],
                'user.name',
                false
            ],
            [
                [
                    'user' => [
                        'name' => 'Mike'
                    ]
                ],
                'user.name',
                true
            ],
            [
                [
                    'user' => [
                        'name' => null
                    ]
                ],
                'user.name',
                true
            ],
            [
                [

                    'user' => [
                        'names' => []
                    ]

                ],
                'user.names',
                true
            ],
            [
                [

                    'user' => [
                        'names' => []
                    ]

                ],
                'user.names.0',
                false
            ],
            [
                [

                    'user' => [
                        'names' => ['Mike']
                    ]

                ],
                'user.names.0',
                true
            ],

        ];
    }

    /**
     * @dataProvider itAddsElementDataProvider
     */
    public function testItAddsElement(array $array, string|int $pathKey, mixed $value, array $expectedResult): void
    {
        $wrapper = ArrayWrapper::create($array);
        $wrapper->addElement($pathKey, $value);
        self::assertEquals($expectedResult, $wrapper->getElement($pathKey));
    }

    public function itAddsElementDataProvider(): array
    {
        return [
            [
                [],
                0,
                'Mike',
                ['Mike']
            ],
            [
                [
                    0 => [
                        'Nick'
                    ]
                ],
                0,
                'Mike',
                ['Nick', 'Mike']
            ],
        ];
    }

    public function testItDoesNotAddElement(): void
    {
        $wrapper = ArrayWrapper::create([
            'user' => [
                'name' => 'Mike'
            ]
        ]);
        $this->expectException(ElementIsNotAnArrayException::class);
        $this->expectExceptionMessage('Element [user.name] is not an array!');
        $wrapper->addElement('user.name', 'Nick');
    }

    public function testItGetsFirstElement(): void
    {
        $wrapper = ArrayWrapper::create([1, 2, 3, 4, 5]);
        self::assertEquals(1, $wrapper->getFirstElement());

        $wrapper = ArrayWrapper::create([]);
        self::assertEquals('default', $wrapper->getFirstElement('default'));
    }

    public function testItGetsLastElement(): void
    {
        $wrapper = ArrayWrapper::create([1, 2, 3, 4, 5]);
        self::assertEquals(5, $wrapper->getLastElement());

        $wrapper = ArrayWrapper::create([]);
        self::assertEquals('default', $wrapper->getLastElement('default'));
    }

    /**
     * @dataProvider isAssociativeDataProvider
     */
    public function testIsAssociative(array $array, bool $expectedResult): void
    {
        $wrapper = ArrayWrapper::create($array);
        self::assertEquals($expectedResult, $wrapper->isAssociative());
    }

    public function testItIterates(): void
    {
        $array = ['a', 'b', 'c'];
        $wrapper = ArrayWrapper::create($array);
        foreach ($wrapper as $key => $value) {
            self::assertEquals($value, $array[$key]);
        }
    }

    public function isAssociativeDataProvider(): array
    {
        return [
            [
                [],
                false
            ],
            [
                [1, 2, 3],
                false
            ],
            [
                [
                    0 => 1,
                    1 => 2,
                    2 => 3
                ],
                false
            ],
            [
                [
                    0 => 1,
                    2 => 3
                ],
                false
            ],
            [
                [
                    'a' => 1,
                    'b' => 2
                ],
                true
            ],
            [
                [
                    'a' => 1,
                    1 => 2
                ],
                true
            ],
            [
                [
                    '0' => 1,
                    '1' => 2
                ],
                false
            ]
        ];
    }

    /**
     * @dataProvider itRemovesElementDataProvider
     */
    public function testItRemovesElement(array $array, int|string $pathKey, array $expectedResult): void
    {
        $wrapper = ArrayWrapper::create($array);
        $wrapper->removeElement($pathKey);
        self::assertEquals($expectedResult, $wrapper->getWrapped());
    }

    public function itRemovesElementDataProvider(): array
    {
        return [
            [
                [1, 2, 3],
                1,
                [
                    0 => 1,
                    2 => 3
                ]
            ],
            [
                [
                    'user' => [
                        'name' => 'Mike',
                        'age' => 25
                    ]
                ],
                'user.name',
                [
                    'user' => [
                        'age' => 25
                    ]
                ]
            ],
            [
                [
                    'user' => [
                        'names' => ['Mike', 'Michael'],
                        'age' => 25
                    ]
                ],
                'user.names.0',
                [
                    'user' => [
                        'names' => [
                            1 => 'Michael'
                        ],
                        'age' => 25
                    ]
                ]
            ]
        ];
    }

    public function testSetElementAsNotPath(): void
    {
        $wrapper = ArrayWrapper::create();
        $wrapper->setElement('user.name', 'Joe', false);
        self::assertEquals(
            ['user.name' => 'Joe'],
            $wrapper->getWrapped()
        );
    }

    public function testAddElementAsNotPath(): void
    {
        $wrapper = ArrayWrapper::create();
        $wrapper->addElement('user.name', 'Joe', false);
        self::assertEquals(
            ['user.name' => ['Joe']],
            $wrapper->getWrapped()
        );
    }

    public function testGetElementAsNotPath(): void
    {
        $wrapper = ArrayWrapper::create([
            'user.name' => 'Joe'
        ]);
        self::assertEquals(
            'Joe',
            $wrapper->getElement('user.name', keyAsPath: false)
        );
    }

    public function testRemoveElementAsNotPath(): void
    {
        $wrapper = ArrayWrapper::create([
            'user.name' => 'Joe'
        ]);
        $wrapper->removeElement('user.name', false);
        self::assertEquals(
            [],
            $wrapper->getWrapped()
        );
    }

    public function testHasElementAsNotPath(): void
    {
        $wrapper = ArrayWrapper::create([
            'user.name' => 'Joe'
        ]);
        self::assertFalse($wrapper->hasElement('user.name'));
        self::assertTrue($wrapper->hasElement('user.name', false));
    }

    /**
     * @dataProvider containsArrayDataProvider
     */
    public function testContainsArray(array $haystack, array $needle, bool $expectedResult): void
    {
        $wrapper = ArrayWrapper::create($haystack);
        self::assertEquals($expectedResult, $wrapper->containsArray($needle));
    }

    public function containsArrayDataProvider(): array
    {
        return [
            'not associative partial success' => [
                [1, 2, 3],
                [1],
                true
            ],
            'not associative full success' => [
                [1, 2, 3],
                [1, 2, 3],
                true
            ],
            'not associative multidimensional partial success' => [
                [1, 2, 3, ['a', 'b', 'c']],
                [1, 2, 3, ['a']],
                true
            ],
            'not associative multidimensional full success' => [
                [1, 2, 3, ['a', 'b', 'c']],
                [1, 2, 3, ['a', 'b', 'c']],
                true
            ],
            'not associative failure' => [
                [1, 2, 3],
                [5],
                false
            ],
            'not associative position failure' => [
                [1, 2, 3],
                [2, 1, 3],
                false
            ],
            'not associative extra data failure' => [
                [1, 2, 3],
                [1, 2, 3, 4],
                false
            ],
            'not associative multi dimension failure' => [
                [1, 2, 3, [1, 2, 3]],
                [1, 2, 3, [2]],
                false
            ],
            'associative full success' => [

                [
                    'id' => 1,
                    'name' => 'Test'
                ],
                [
                    'name' => 'Test',
                    'id' => 1,

                ],
                true
            ],
            'associative partial success' => [
                [
                    'id' => 1,
                    'name' => 'Test'
                ],
                [
                    'id' => 1,
                ],
                true
            ],
            'associative value fail' => [
                [
                    'id' => 1,
                    'name' => 'Test'
                ],
                [
                    'id' => 2,
                ],
                false,

            ],
            'associative key fail' => [

                [
                    'id' => 1,
                    'name' => 'Test'
                ],
                [
                    'identifier' => 1,
                ],
                false,

            ],
            'associative extra date' => [
                [
                    'id' => 1,
                    'name' => 'Test'
                ],
                [
                    'id' => 1,
                    'name' => 'Test',
                    'alias' => 'Test'
                ],
                false

            ],
            'associative multi dimension success' => [
                [
                    'id' => 1,
                    'name' => 'Test',
                    'address' => [
                        'city' => 'New York',
                        'street' => 'Wall Street'
                    ]
                ],
                [
                    'id' => 1,
                    'name' => 'Test',
                    'address' => [
                        'city' => 'New York',
                    ]
                ],
                true
            ],
        ];
    }

    /**
     * @dataProvider getAsStringOrNullDataProvider
     * @param class-string|string|null $expectedResult
     */
    public function testGetAsStringOrNull(
        mixed $value,
        string|null $expectedResult,
        ?string $defaultValue = null,
    ): void {
        if (is_string($expectedResult) && class_exists($expectedResult)) {
            $this->expectException($expectedResult);
            ArrayWrapper::create(['value' => $value])->getAsStringOrNull('value', $defaultValue);
        } else {
            self::assertEquals(
                $expectedResult,
                ArrayWrapper::create(['value' => $value])->getAsStringOrNull('value', $defaultValue)
            );
        }
    }

    public function getAsStringOrNullDataProvider(): array
    {
        return [
            'string' => [
                'text',
                'text'
            ],
            'default value' => [
                null,
                'default',
                'default',
            ],
            'int' => [
                1,
                '1'
            ],
            'float' => [
                1.01,
                '1.01'
            ],
            'false' => [
                false,
                ''
            ],
            'true' => [
                true,
                '1'
            ],
            'null' => [
                null,
                null
            ],
            'stringable object' => [
                new class {
                    public function __toString(): string
                    {
                        return 'text';
                    }
                },
                'text'
            ],
            'not stringable object' => [
                new class {
                },
                CannotGetAsStringException::class
            ],
            'array' => [
                [],
                CannotGetAsStringException::class
            ]
        ];
    }

    /**
     * @dataProvider getAsFloatOrNullDataProvider
     * @param class-string|null|float $expectedResult
     */
    public function testGetAsFloatOrNull(
        mixed $value,
        float|string|null $expectedResult,
        ?float $defaultValue = null,
    ): void {
        if (is_string($expectedResult) && class_exists($expectedResult)) {
            $this->expectException($expectedResult);
            ArrayWrapper::create(['value' => $value])->getAsFloatOrNull('value', $defaultValue);
        } else {
            self::assertEquals(
                $expectedResult,
                ArrayWrapper::create(['value' => $value])->getAsFloatOrNull('value', $defaultValue)
            );
        }
    }

    public function getAsFloatOrNullDataProvider(): array
    {
        return [
            'text string' => [
                'text',
                CannotGetAsFloatException::class
            ],
            'int string' => [
                '1',
                1.0
            ],
            'float string' => [
                '1.01',
                1.01
            ],
            'default value' => [
                null,
                1.01,
                1.01,
            ],
            'int' => [
                1,
                1,
            ],
            'float' => [
                1.01,
                1.01,
            ],
            'bool' => [
                false,
                CannotGetAsFloatException::class
            ],
            'null' => [
                null,
                null,
            ],
            'stringable object' => [
                new class {
                    public function __toString(): string
                    {
                        return '1.07';
                    }
                },
                1.07
            ],
            'not stringable object' => [
                new class {
                },
                CannotGetAsFloatException::class
            ],
            'array' => [
                [],
                CannotGetAsFloatException::class
            ]
        ];
    }

    /**
     * @dataProvider getAsIntOrNullDataProvider
     * @param class-string|null|int $expectedResult
     */
    public function testGetAsIntOrNull(
        mixed $value,
        int|string|null $expectedResult,
        ?int $defaultValue = null,
    ): void {
        if (is_string($expectedResult) && class_exists($expectedResult)) {
            $this->expectException($expectedResult);
            ArrayWrapper::create(['value' => $value])->getAsIntOrNull('value', $defaultValue);
        } else {
            self::assertEquals(
                $expectedResult,
                ArrayWrapper::create(['value' => $value])->getAsIntOrNull('value', $defaultValue)
            );
        }
    }

    public function getAsIntOrNullDataProvider(): array
    {
        return [
            'text string' => [
                'text',
                CannotGetAsIntException::class
            ],
            'int string' => [
                '1',
                1
            ],
            'float string' => [
                '1.01',
                CannotGetAsIntException::class
            ],
            'default value' => [
                null,
                1,
                1,
            ],
            'int' => [
                1,
                1,
            ],
            'float' => [
                1.01,
                CannotGetAsIntException::class
            ],
            'bool' => [
                false,
                CannotGetAsIntException::class
            ],
            'null' => [
                null,
                null,
            ],
            'stringable object' => [
                new class {
                    public function __toString(): string
                    {
                        return '1';
                    }
                },
                1
            ],
            'not stringable object' => [
                new class {
                },
                CannotGetAsIntException::class
            ],
            'array' => [
                [],
                CannotGetAsIntException::class
            ]
        ];
    }


    /**
     * @dataProvider getAsBoolOrNullDataProvider
     * @param class-string|null|bool $expectedResult
     */
    public function testGetAsBoolOrNull(
        mixed $value,
        bool|string|null $expectedResult,
        ?bool $defaultValue = null,
    ): void {
        if (is_string($expectedResult) && class_exists($expectedResult)) {
            $this->expectException($expectedResult);
            ArrayWrapper::create(['value' => $value])->getAsBoolOrNull('value', $defaultValue);
        } else {
            self::assertEquals(
                $expectedResult,
                ArrayWrapper::create(['value' => $value])->getAsBoolOrNull('value', $defaultValue)
            );
        }
    }

    public function getAsBoolOrNullDataProvider(): array
    {
        return [
            'text string' => [
                'text',
                CannotGetAsBoolException::class
            ],
            'int string' => [
                '3',
                CannotGetAsBoolException::class
            ],
            'float string' => [
                '1.01',
                CannotGetAsBoolException::class
            ],
            'default value' => [
                null,
                false,
                false,
            ],
            'int' => [
                5,
                CannotGetAsBoolException::class,
            ],
            'float' => [
                1.01,
                CannotGetAsBoolException::class
            ],
            'bool' => [
                false,
                false
            ],
            'text true' =>
                [
                    'true',
                    true
                ],
            'text false' =>
                [
                    'false',
                    false
                ],
            'text True' =>
                [
                    'True',
                    true
                ],
            'text False' =>
                [
                    'False',
                    false
                ],
            'text TRUE' =>
                [
                    'TRUE',
                    true
                ],
            'text FALSE' =>
                [
                    'FALSE',
                    false
                ],
            'int 1' =>
                [
                    1,
                    true
                ],
            'int 0' => [
                0,
                false
            ],
            'text 1' => [
                '1',
                true
            ],
            'text 0' => [
                '0',
                false
            ],
            'null' => [
                null,
                null,
            ],
            'stringable object' => [
                new class {
                    public function __toString(): string
                    {
                        return 'true';
                    }
                },
                true
            ],
            'not stringable object' => [
                new class {
                },
                CannotGetAsBoolException::class
            ],
            'array' => [
                [],
                CannotGetAsBoolException::class
            ]
        ];
    }

    /**
     * @dataProvider getAsArrayOrNullDataProvider
     * @param class-string|null|array $expectedResult
     */
    public function testGetAsArrayOrNull(
        mixed $value,
        array|string|null $expectedResult,
        ?array $defaultValue = null,
    ): void {
        if (is_string($expectedResult) && class_exists($expectedResult)) {
            $this->expectException($expectedResult);
            ArrayWrapper::create(['value' => $value])->getAsArrayOrNull('value', $defaultValue);
        } else {
            self::assertEquals(
                $expectedResult,
                ArrayWrapper::create(['value' => $value])->getAsArrayOrNull('value', $defaultValue)
            );
        }
    }

    public function getAsArrayOrNullDataProvider(): array
    {
        return [
            'not array' => [
                'text',
                CannotGetAsArrayException::class
            ],
            'default value' => [
                null,
                [1, 2, 3],
                [1, 2, 3],
            ],
            'array' => [
                [1, 2, 3],
                [1, 2, 3],
            ],
        ];
    }

    public function testGetAsString(): void
    {
        self::assertEquals('text', ArrayWrapper::create(['value' => 'text'])->getAsString('value'));
        self::assertEquals('text', ArrayWrapper::create()->getAsString('value', 'text'));
        $this->expectException(CannotGetAsStringException::class);
        ArrayWrapper::create()->getAsString('value');
    }

    public function testGetAsFloat(): void
    {
        self::assertEquals(1.01, ArrayWrapper::create(['value' => 1.01])->getAsFloat('value'));
        self::assertEquals(1.01, ArrayWrapper::create()->getAsFloat('value', 1.01));
        $this->expectException(CannotGetAsFloatException::class);
        ArrayWrapper::create()->getAsFloat('value');
    }

    public function testGetAsInt(): void
    {
        self::assertEquals(1, ArrayWrapper::create(['value' => 1])->getAsInt('value'));
        self::assertEquals(1, ArrayWrapper::create()->getAsInt('value', 1));
        $this->expectException(CannotGetAsIntException::class);
        ArrayWrapper::create()->getAsInt('value');
    }

    public function testGetAsBool(): void
    {
        self::assertTrue(ArrayWrapper::create(['value' => true])->getAsBool('value'));
        self::assertTrue(ArrayWrapper::create()->getAsBool('value', true));
        $this->expectException(CannotGetAsBoolException::class);
        ArrayWrapper::create()->getAsBool('value');
    }

    public function testGetAsArray(): void
    {
        self::assertEquals([1, 2, 3], ArrayWrapper::create(['value' => [1, 2, 3]])->getAsArray('value'));
        self::assertEquals([1, 2, 3], ArrayWrapper::create()->getAsArray('value', [1, 2, 3]));
        $this->expectException(CannotGetAsArrayException::class);
        ArrayWrapper::create()->getAsArray('value');
    }


}
